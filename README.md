# TamTam Frontend Assignment #

A simple demo web app build with Sass, React and Webpack for TamTam.

### Information ###

* Author: Kees Hak
* Version: 0.1.0
* Repository: https://keeshak@bitbucket.org/keeshak/tamtam-frontend-assignment.git

### Requirements ###

* Node.js
* Webpack

### Configuration ###

* $ npm install
* $ webpack
