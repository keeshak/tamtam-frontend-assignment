import React, { Component } from 'react';
import Alert from './alert.js';

class Form extends Component {
    constructor() {
        super();

        this.state = {
            formFields: {
                firstname: {name: 'firstname', type: 'text', placeholder: 'First name', notification: 'We need your first name.', required: true},
                lastname: {name: 'lastname', type: 'text', placeholder: 'Last name', notification: 'We need your last name.', required: true},
                email: {name: 'email', type: 'email', placeholder: 'Your e-mail address', notification: 'Please use a valid e-mail address.', required: true},
                phone: {name: 'phone', type: 'text', placeholder: 'Your phone number (optional)'},
                message: {name: 'message', type: 'textarea', placeholder: 'Your message...', notification: 'Sorry, your message can\'t be empty.', required: true}},
            isValid: true
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange() {
        this.validateForm();
    }

    handleSubmit(e) {
        e.preventDefault();
        this.validateForm();
    }

    validateForm() {
        let isValid = true;

        Object.entries(this.state.formFields).forEach(([i, field]) => {
            this.state.formFields[field.name].error = false;

            if (!this.validateInput(field)) {
                isValid = false;
                this.state.formFields[field.name].error = true;
            }
        });

        this.setState({isValid: isValid});
    }

    validateInput(field) {
        const ref = this.refs[field.name];
        const validity = ref.validity;

        ref.classList.add('form__input--validated');

        if (!validity.valid) {
            return false;
        }

        return true;
    }

    renderAlert() {
        const isValid = this.state.isValid;

        if (!isValid) {
            return (
                <Alert message="Please complete the form and try again." />
            );
        }
    }

    renderInput(field) {
        switch (field.type) {
            case 'textarea':
                return (
                    <div className="form__group form__group--textarea">
                        <textarea className="form__input form__input--textarea"
                                  name={field.name}
                                  ref={field.name}
                                  placeholder={field.placeholder}
                                  onChange={this.handleChange}
                                  required={field.required ? true : false} />
                        {field.error ? (<div className="form__error">{field.notification}</div>) : ('')}
                    </div>
                );

                break;
            case 'text':
            default:
                return (
                    <div className="form__group">
                        <input className="form__input form__input--text"
                               type={field.type}
                               name={field.name}
                               ref={field.name}
                               placeholder={field.placeholder}
                               onChange={this.handleChange}
                               required={field.required ? true : false} />
                        {field.error ? (<div className="form__error">{field.notification}</div>) : ('')}
                    </div>
                );

                break;
        }
    }

    render() {
        let formFields = [];
        Object.entries(this.state.formFields).forEach(([i, field]) => formFields.push(field));

        return (
            <form className="form" noValidate>
                {this.renderAlert()}
                <div className="form__wrapper">
                    {formFields.map(field => this.renderInput(field))}
                </div>
                <div className="form__submit">
                    <button className="btn btn--success" onClick={this.handleSubmit}>Send</button>
                </div>
            </form>
        );
    }
}

export default Form;
