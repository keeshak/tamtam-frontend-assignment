import React, { Component } from 'react';
import FacebookIcon from '../images/icons/facebook.svg';
import InstagramIcon from '../images/icons/instagram.svg';
import TwitterIcon from '../images/icons/twitter.svg';

class Social extends Component {
    render() {
        return (
            <nav className="social">
                <ul className="social__list">
                    <li className="social__item">
                        <a className="social__link" href="https://www.facebook.com/DeptAgency/" target="_blank"><FacebookIcon /></a>
                    </li>

                    <li className="social__item">
                        <a className="social__link" href="https://twitter.com/deptagency" target="_blank"><TwitterIcon /></a>
                    </li>

                    <li className="social__item">
                        <a className="social__link" href="https://www.instagram.com/deptagency/" target="_blank"><InstagramIcon /></a>
                    </li>
                </ul>
            </nav>
        );
    }
}

export default Social;
