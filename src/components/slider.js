import React, { Component } from 'react';
import Swipe, { SwipeItem } from 'swipejs/react';
import LeftIcon from '../images/icons/left.svg';
import RightIcon from '../images/icons/right.svg';

class Slider extends Component {
    constructor() {
        super();

        this.swipeLeft = this.swipeLeft.bind(this);
        this.swipeRight = this.swipeRight.bind(this);
    }

    swipeLeft() {
        this.swipe.prev();
    }

    swipeRight() {
        this.swipe.next();
    }

    render() {
        let slideImage = [];
        slideImage[0] = require('../images/slides/walibi.jpg');
        slideImage[1] = require('../images/slides/florensis.jpg');
        slideImage[2] = require('../images/slides/oxxio.png');

        return (
            <div className="slider">
                <Swipe ref={slider => this.swipe = slider}
                       continuous={true}
                       draggable={false}
                       speed={300}>
                    <SwipeItem className="slider__slide"><figure className="slider__img" style={{backgroundImage: `url(${slideImage[0]})`}} /></SwipeItem>
                    <SwipeItem className="slider__slide"><figure className="slider__img" style={{backgroundImage: `url(${slideImage[1]})`}} /></SwipeItem>
                    <SwipeItem className="slider__slide"><figure className="slider__img" style={{backgroundImage: `url(${slideImage[2]})`}} /></SwipeItem>
                </Swipe>
                <nav className="slider__nav">
                    <button className="btn btn--primary btn--icon slider__prev" onClick={this.swipeLeft}><LeftIcon /></button>
                    <button className="btn btn--primary slider__btn">View case</button>
                    <button className="btn btn--primary btn--icon slider__next" onClick={this.swipeRight}><RightIcon /></button>
                </nav>
            </div>
        );
    }
}

export default Slider;
