import React, { Component } from 'react';

class Instafeed extends Component {
    constructor() {
        super();

        this.state = {
            error: null,
            images: [],
            isLoaded: false
        };
     }

    componentDidMount() {
        const query = 'users/self/media/recent';
        const limit = 6;
        const token = '7536562331.c936f6d.d5e17d2504cc4d51a0650492c8253560';

        fetch(`https://api.instagram.com/v1/${query}?count=${limit}&access_token=${token}`)
            .then(result => result.json())
            .then(result => {
                this.setState({
                    images: result.data,
                    isLoaded: true
                });
            },
            (error) => {
                this.setState({
                    error,
                    isLoaded: true
                });
            }
        )
    }

    renderFeed() {
        const { error, images, isLoaded } = this.state;

        if (!error && isLoaded) {
            return (
                images.map(image => (
                    <figure className="instafeed__figure">
                        <img src={image.images.low_resolution.url} className="instafeed__image" />
                        <p className="instafeed__caption">{image.caption.text}</p>
                    </figure>
                ))
            );
        }
    }

    render() {
        return (
            <div className="instafeed">
                <h3 className="instafeed__title">Follow us on Instagram</h3>
                <h4 className="instafeed__subtitle">@tamtamnl</h4>
                <div className="instafeed__list">
                    {this.renderFeed()}
                </div>
            </div>
        );
    }
}

export default Instafeed;
