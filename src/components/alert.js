import React, { Component } from 'react';
import ErrorIcon from '../images/icons/error.svg';

class Alert extends Component {
    render() {
        const {message, ...props} = this.props;

        return (
            <div className="alert alert--warning">
                <figure className="alert__icon"><ErrorIcon /></figure>
                <p className="alert__message">{message}</p>
            </div>
        );
    }
}

export default Alert;
