import React, { Component } from 'react';
import { BrowserRouter as Router, NavLink } from 'react-router-dom';
import CloseIcon from '../images/icons/close.svg';
import MenuIcon from '../images/icons/menu.svg';

class Menu extends Component {
    constructor() {
        super();

        this.state = {
            mobileMenuOpen: false,
            windowWidth: window.innerWidth
        };

        this.toggleMenu = this.toggleMenu.bind(this);
    }

    componentWillMount() {
        window.addEventListener('resize', this.windowSizeChange.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.windowSizeChange.bind(this));
    }

    windowSizeChange() {
        this.setState({windowWidth: window.innerWidth});
    };

    toggleMenu() {
        this.setState({mobileMenuOpen: !this.state.mobileMenuOpen});
    }

    renderMenu(menuClass) {
        return (
            <nav className={menuClass}>
                <ul className={`${menuClass}__list`}>
                    <li className={`${menuClass}__item`}>
                        <NavLink exact to="/" className={`${menuClass}__link`} activeClassName={`${menuClass}__link--active`}>Home</NavLink>
                    </li>
                    <li className={`${menuClass}__item`}>
                        <NavLink to="/people" className={`${menuClass}__link`} activeClassName={`${menuClass}__link--active`}>People</NavLink>
                    </li>
                    <li className={`${menuClass}__item`}>
                        <NavLink to="/contact" className={`${menuClass}__link`} activeClassName={`${menuClass}__link--active`}>Contact</NavLink>
                    </li>
                </ul>
            </nav>
        );
    }

    render() {
        const { mobileMenuOpen, windowWidth } = this.state;
        const isMobile = windowWidth < 480;

        if (isMobile) {
            return (
                <div>
                    <button className="mobile-menu__trigger" onClick={this.toggleMenu}>{mobileMenuOpen ? <CloseIcon /> : <MenuIcon />}</button>
                    {mobileMenuOpen ? this.renderMenu('mobile-menu') : ('')}
                </div>
            );
        } else {
            return (
                this.renderMenu('menu')
            );
        }
    }
}

export default Menu;
