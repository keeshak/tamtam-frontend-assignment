import React, { Component } from 'react';
import Footer from './footer.js';
import Form from './form.js';
import Header from './header.js';

class Contact extends Component {
    componentDidMount() {
        document.title = 'Contact - TamTam Frontend Assignment';
    }

    render() {
        return (
            <div>
                <Header />
                <div className="contact">
                    <h2 className="contact__title">We would love to<br />hear from you</h2>
                    <Form />
                </div>
                <Footer />
            </div>
        );
    }
}

export default Contact;
