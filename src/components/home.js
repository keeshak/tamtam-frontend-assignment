import React, { Component } from 'react';
import Content from './content.js';
import Footer from './footer.js';
import Header from './header.js';
import Instafeed from './instafeed.js';
import Slider from './slider.js';

class Home extends Component {
    componentDidMount() {
        document.title = 'Home - TamTam Frontend Assignment';
    }

    render() {
        return (
            <div>
                <Header />
                <Slider />
                <Content />
                <Instafeed />
                <Footer />
            </div>
        );
    }
}

export default Home;
