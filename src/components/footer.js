import React, { Component } from 'react';
import Social from './social.js';

class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <Social />
            </footer>
        );
    }
}

export default Footer;
