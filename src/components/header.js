import React, { Component } from 'react';
import Logo from '../images/logo.svg';
import Menu from './menu.js';

class Header extends Component {
    render() {
        return (
            <header className="header">
                <div className="header__wrapper">
                    <figure className="header__logo"><Logo /></figure>
                    <Menu />
                </div>
                <div className="header__placeholder"></div>
            </header>
        );
    }
}

export default Header;
