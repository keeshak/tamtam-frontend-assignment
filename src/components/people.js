import React, { Component } from 'react';
import Footer from './footer.js';
import Header from './header.js';

class People extends Component {
    componentDidMount() {
        document.title = "People - TamTam Frontend Assignment";
    }

    render() {
        return (
            <div>
                <Header />
                <Footer />
            </div>
        );
    }
}

export default People;
