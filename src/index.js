import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Contact from './components/contact.js';
import Home from './components/home.js';
import People from './components/people.js';
import './index.scss';

ReactDOM.render((
    <Router>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/people" component={People}/>
            <Route path="/contact" component={Contact}/>
        </Switch>
    </Router>
), document.getElementById('root'));
