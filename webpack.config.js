const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: path.resolve(__dirname, 'src') + '/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'javascript.js'
    },
    module: {
        rules: [
            {
                test: /\.(gif|jpg|png)$/,
                include: path.resolve(__dirname, 'src'),
                use: ['url-loader']
            },
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src'),
                use: ['babel-loader', 'eslint-loader']
            },
            {
                test: /\.scss$/,
                include: path.resolve(__dirname, 'src'),
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.svg$/,
                include: path.resolve(__dirname, 'src'),
                use: ['react-svg-loader']
            }
        ]
    }
};
